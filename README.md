# OpenCore-AMD

Opencore version used: 0.7.6-Release\
CPU: AMD Ryzen 5 2600X\
GPU: AMD RX 5600XT\
RAM: 16GB DDR4\
Motherboard: MSI X470 Gaming Plus\
Audio Codec: ALC892\
Ethernet Card: Realtek 8111H Gigabit\
Wifi/BT Card: N/A\
Touchpad and touch display devices: N/A\
BIOS Version: A.K2\
\
Guide used: Dortania\
\
What's working:\
Hardware Acceleration using GPU\
DRM protected videos\
iServices\
Speakers and Headphones/Earphones\
Sleep\
\
What's not working:\
Microphone (The guide mentions that it doesn't work on AMD systems without VoodooHDA so sadge)\
Facetime does not detect virtual cameras, had to use an USB camera for it to work\
\
NOTE: When I first booted from the USB, it did not show the macOS option. I had to press space to show all options and boot from the entry having "dmg" in it.\
\
NOTE 1: I have installed macOS on an HDD and it takes around a minute to boot completely. Haven't been able to reduce boot time more than this so far.
